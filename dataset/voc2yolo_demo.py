from __future__ import division, print_function
from decimal import Decimal
import xml.etree.ElementTree as ET
import pickle
import os
from os import listdir, getcwd
from os.path import join
from random import shuffle

def checkpath(path):
    if not os.path.exists(path):
        os.makedirs(path)

#sets=[('2012', 'train'), ('2012', 'val'), ('2007', 'train'), ('2007', 'val'), ('2007', 'test')]
#classes = ["aeroplane", "bicycle", "bird", "boat", "bottle", "bus", "car", "cat", "chair", "cow", "diningtable", "dog", "horse", "motorbike", "person", "pottedplant", "sheep", "sofa", "train", "tvmonitor"]

sets=[('2019', 'train'), ('2019', 'val')]
classes = ["person"]

mapping_file = 'mapping.txt'
if os.path.isfile(mapping_file):
    print("mapping.txt is provided, try to read it")
    with open(mapping_file) as f:
        lines = f.read().splitlines()
        classes = [x.strip() for x in lines]
else:
    print("mapping.txt is not provided...use default")

print("classes num: ", len(classes))

classes_count = {x:0 for x in classes}


def convert(size, box):
    dw = 1./size[0]
    dh = 1./size[1]
    x = (box[0] + box[1])/2.0
    y = (box[2] + box[3])/2.0
    w = box[1] - box[0]
    h = box[3] - box[2]
    x = x*dw
    w = w*dw
    y = y*dh
    h = h*dh
    return (x,y,w,h)

def convert_annotation(year, image_id):
    out_file = open('LIST/labels/%s.txt'%(image_id), 'w')
    try:
        in_file = open('Annotations/%s.xml'%(image_id))
    except:
        print("image {} has no label, set as background".format(image_id))
        return
    tree=ET.parse(in_file)
    root = tree.getroot()
    size = root.find('size')
    w = int(size.find('width').text)
    h = int(size.find('height').text)

    for obj in root.iter('object'):
        difficult = obj.find('difficult').text
        cls = obj.find('name').text
        if cls not in classes or int(difficult) == 1:
            print("{:}, class: {:} not in classes list".format(image_id,cls))
            continue
        cls_id = classes.index(cls)
        classes_count[cls] += 1 # count the classes number
        xmlbox = obj.find('bndbox')
        b = (float(xmlbox.find('xmin').text), float(xmlbox.find('xmax').text), float(xmlbox.find('ymin').text), float(xmlbox.find('ymax').text))
        bb = convert((w,h), b)

        if not (bb[0]>1e-09 and bb[1]>1e-09 and bb[2]>1e-09 and bb[3]>1e-09) and (bb[0]+bb[2]/2 < 1) and (bb[1]+bb[3]/2 < 1):
            print("image id: ", image_id)
            print("value: ",bb)
            print("x+w/2 < 1: ",bb[0]+bb[2]/2)
            print("y+h/2 < 1: ",bb[1]+bb[3]/2)
            
            #raise ValueError(' value not in range!!!!!!')
        else:
            out_file.write(str(cls_id) + " " + " ".join([str(a) for a in bb]) + '\n')


wd = getcwd()


# write train and val set

all_image_List = os.listdir("JPEGImages")
shuffle(all_image_List)
#print(all_image_List)
print("total image num: ", len(all_image_List))


n_train = int(len(all_image_List)*0.9)
train_list = all_image_List[:n_train]
val_list = all_image_List[n_train:]
print("train: {}, val:{}".format(len(train_list), len(val_list)))
checkpath("ImageSets/Main")
# write name with extension.
with open("ImageSets/Main/train.txt", "w") as file:
    for item in train_list:
        file.write("%s\n" % item)
with open("ImageSets/Main/val.txt", "w") as file:
    for item in val_list:
        file.write("%s\n" % item)

'''
image_set = 'train'
image_ids = open('ImageSets/Main/%s.txt'%(image_set)).read().strip().split()
for image_id in image_ids:
    print(image_id.split(".")[0])
    print(os.path.splitext(image_id)[0])
'''

# start
for year, image_set in sets:
    checkpath('LIST/labels/')

    image_ids = open('ImageSets/Main/%s.txt'%(image_set)).read().strip().split()
    list_file = open('LIST/%s_%s.txt'%("yolo", image_set), 'w')
    for image_id in image_ids:
        # list_file.write('%s/JPEGImages/%s.jpg\n'%(wd, image_id))
        list_file.write('%s/JPEGImages/%s\n'%(wd, image_id)) #with extension.
        if image_id.split(".")[0] == '':
            print(image_id)
            print("===============================")
        convert_annotation(year, image_id.split(".")[0]) # basename
    list_file.close()

# concat txt to all.

filenames = ['LIST/yolo_train.txt', 'LIST/yolo_val.txt']
with open('LIST/yolo_trainval.txt', 'w') as outfile:
    for fname in filenames:
        with open(fname) as infile:
            outfile.write(infile.read())

# make a copy of label to root
checkpath('labels/')
all_label_List = os.listdir("LIST/labels/")
for item in all_label_List:
    # print(wd, "LIST/labels/", item)
    # print(wd, "labels/", item)
    os.symlink(os.path.join(wd, "LIST/labels/", item), os.path.join(wd, "labels/", item))

from collections import OrderedDict
import operator
print("\nnumber of objects for each class:\n")
d_descending = sorted(classes_count.iteritems(), 
                                  key=lambda (k,v): (v,k), reverse=True)
for k, v in d_descending:
    print("{}: {}".format(k,v))

print("\nconvert voc sucessfully to yolo")
print("please copy the yolo_train.txt, yolo_tranval.txt, yolo_val.txt file under LIST to the train folder\n")
print("remember to modify the .cfg, .data and .names files")
print("in each [yolo] layer:\n  classes={:}".format(len(classes)))
print("  anchors= what you calculate or default\n")
print("in each [convolutional] layer before [yolo] layer\n  filters={:}\n".format((int(len(classes))+5)*3))
with open('yolo_info.txt', 'w') as output_info:
    output_info.write(str(len(classes)))