# Maintainer YT

## Initiation for new docker

$ rm \*/*/.gitignore  

$ pip install matplotlib  

$ tar -xvf yolo_train/darknet_alexey.tar.xz  
$ cd yolo_train/darknet_alexey  
$ make  

## TRAINING
### dataset
1. $ make clean
2. copy training images to ./JPEGImages
3. copy training annotations (.xml) to ./Annotations [ensure corresponding has the same name]
4. modify the ./mapping.txt with appropriate labels/classes (ensure no extra \n at EOF)
5. $ python voc2yolo_demo.py
6. $ make update
7. $ make target

### yolo_train
1. $ bash train.sh


## DEMO
### demo
1. calculate the best weight
2. copy yolo_train/cfg/yolov3_deploy.cfg to demo/models/yolov3_deploy.cfg
3. copy yolo_train/backup/.weight to demo/models/yolov3_deploy.weights
4. copy yolo_train/data/yolo.names to demo/data/deploy.names
5. edit classes number in demo/data/deploy.data

