darknet=darknet_alexey/darknet
cfgFile="data/calc_anchors.data"

$darknet detector calc_anchors $cfgFile -num_of_clusters 9 -width 608 -height 608

# for tiny 
#$darknet detector calc_anchors  $cfgFile -num_of_clusters 6 -width 416 -height 416
