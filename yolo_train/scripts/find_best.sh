#CUDA_VISIBLE_DEVICES=0 /media/graymatics/data/lize/code/darknet_alex/darknet detector map data/head.data cfg/yolov3-head_deploy.cfg backup/yolov3-head_3600.weights | tee map.log


#for file in  `find backup/* -type f | sort -V`; do
#  echo ${file##*/}
#done


for file in `ls backup/*.weights | sort -V`; do
  #echo ${file##*/}
  echo "CUDA_VISIBLE_DEVICES=0 /media/graymatics/data/lize/code/darknet_alex/darknet detector map data/head.data cfg/yolov3-head_deploy.cfg backup/${file##*/}"
done;
