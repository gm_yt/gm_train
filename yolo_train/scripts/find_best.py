from __future__ import print_function, division
import os
from subprocess import call,check_output
from subprocess import Popen, PIPE
import re
import json
import time


my_env = os.environ.copy()
my_env["CUDA_VISIBLE_DEVICES"]="0"

#gpu_id = 0
#gpu = "CUDA_VISIBLE_DEVICES=0"
detector = 'darknet_alexey/darknet'
cmd1 = 'detector'
cmd2 = 'map'

data_file = 'data/yolo.data'
cfg_file = 'cfg/yolov3_deploy.cfg' # yolov3_multiobjs_deploy yolov3_1obj_deploy
weight_dir = 'backup' 
min_iter = 2000

save_path = 'find_best_yolov3.txt'

weights_files = os.listdir(weight_dir)
weights_files = [weight for weight in os.listdir(weight_dir) if weight.endswith('.weights') and int(weight.split('.')[0].split('_')[-1]) >= min_iter]
#weights_files.sort()
weights_files = sorted(weights_files, key=lambda x: float(x.split('.')[0].split('_')[-1]))
n = len(weights_files)

print(weights_files)
print('total number of valid weights: ', n)

patterns= [r'-?\d+\.?\d*']


res_list = []
total_time = 0

for i in range(n):
    start_time = time.time()
    weights_file = weights_files[i]
    weights_path = os.path.join(weight_dir, weights_file)
    print("{:}/{:}".format(i+1, n))
    print(weights_path)
    iter_value = int(weights_file.split('.')[0].split('_')[-1])
    call(["echo", weights_file])
    CMD = [detector, cmd1, cmd2, data_file, cfg_file, weights_path]
    #print(" ".join(CMD))
    output,error = Popen(CMD,stdout = PIPE, stderr= PIPE, env=my_env).communicate()

    #print(output)
    out = [i for i in output.split('\n') if 'rank' not in i and len(i) != 0]
    print(out)
    #findmatches(patterns, out[2])
    #findmatches(patterns, out[-1])
    match1= re.findall(patterns[0], out[-3]) # precision, recall, f1
    print("match1",match1)
    match2= re.findall(patterns[0], out[-1]) # mAP
    print("match2",match2)
    match3= re.findall(patterns[0], out[-2]) # IoU
    print("match3",match3)
    #out_dict = {'weights':weights_file, 'output':out, 'precision':match1[1], 'recall': match1[2], 'mAP':match2[0]}
    if len(match1) == 5 and len(match2) == 2:
        res_list.append({'weights':weights_file, 'output':out, 'precision':float(match1[1]), 'recall': float(match1[2]),'f1': float(match1[3]),'IoU':float(match3[-1]), 'mAP':float(match2[0])})

    elapsed_time = time.time() - start_time
    total_time += elapsed_time
    print("{}, process time {:.3f} sec".format(weights_file, elapsed_time))

print("total time: {:.3f} sec, avg time: {:.3f} sec".format(total_time, total_time/n))

conditions = ['precision', 'recall', 'f1', 'IoU', 'mAP']
with open(save_path, 'w') as output_file:
    for condi in conditions:
        res_list.sort(key=lambda x: x[condi], reverse=True)
        print("****************{}**********************".format(condi))
        print("TOP 5:\n")
        for i in range(5):
            print("{:}:\n".format(i))
            print(res_list[i])
        print("****************{}**********************".format(condi))
        output_file.write("\n****************{}**********************\n".format(condi))
        output_file.write("TOP 5:\n".format(condi))
        for i in range(5):
            output_file.write("\n{:}:\n".format(i))
            output_file.write(str(res_list[i]))
        output_file.write("\n****************{}**********************\n".format(condi))

