from __future__ import print_function
import argparse
import cv2
import numpy as np
from PIL import Image
import sys, os, shutil
import math
sys.path.append(os.path.join(os.path.dirname(__file__), 'python'))

import darknet_lize as dn
from timer import Timer
from glob import glob

# xml
from lxml.etree import Element, SubElement, tostring
import pprint
from xml.dom.minidom import parseString

def parse_args():
    """Parse input arguments."""
    parser = argparse.ArgumentParser(description='Tensorflow Faster R-CNN demo')
    parser.add_argument('--cfg', dest='cfg_file', help='cfg for darknet',
                         default='cfg/yolov3.cfg')
    parser.add_argument('--weights', dest='weights_file', help='weights for darket',
                         default='weights/yolov3.weights')
    parser.add_argument('--meta', dest='meta_file', help='meta for darknet',
                         default='cfg/coco.data')
    parser.add_argument('--video', dest='video', help='Path to video')
    parser.add_argument('--dir', dest='dir', help='Path to images dir')
    parser.add_argument('--output_dir', dest='output_dir', help='output_dir')
    parser.add_argument('--display', dest='display', help='Display the image/video', action='store_true')
    args = parser.parse_args()

    return args


headstr = """\
<annotation>
    <folder>VOC2007</folder>
    <filename>%s</filename>
    <source>
        <database>My Database</database>
        <annotation>PASCAL VOC2007</annotation>
        <image>flickr</image>
        <flickrid>NULL</flickrid>
    </source>
    <owner>
        <flickrid>NULL</flickrid>
        <name>company</name>
    </owner>
    <size>
        <width>%d</width>
        <height>%d</height>
        <depth>%d</depth>
    </size>
    <segmented>0</segmented>
"""
objstr = """\
    <object>
        <name>%s</name>
        <pose>Unspecified</pose>
        <truncated>0</truncated>
        <difficult>0</difficult>
        <bndbox>
            <xmin>%d</xmin>
            <ymin>%d</ymin>
            <xmax>%d</xmax>
            <ymax>%d</ymax>
        </bndbox>
    </object>
"""

tailstr = '''\
</annotation>
'''
def writexml(xmlpath, head, objects, tail):
    #filename = all_path("Annotations/%06d.xml" % (idx))
    filename = xmlpath
    f = open(filename, "w")
    f.write(head)
    for obj in objects:
        bbx=obj['bndbox']
        f.write(objstr % (obj['name'], bbx[0], bbx[1], bbx[2], bbx[3]))
    f.write(tail)
    f.close()

def checkfilepath(file_path):
    if os.path.isdir(file_path):
        directory = file_path
    else:
        directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)

def writetxt(txt_list, txt_path):
    n = len(txt_list)
    with open(txt_path, 'w') as f:
        for idx in range(n):
            if idx == n-1: # last line
                f.write("%s" % txt_list[idx])
            else: 
                f.write("%s\n" % txt_list[idx])

if __name__ == '__main__':

    args = parse_args()

    video_path = args.video
    root_dir = args.dir
    display = args.display
    output_dir = args.output_dir
    detfile_dir = "det_yolo.txt"
    # set dir for output result
    if root_dir is not None:
        detfile_dir = os.path.join(root_dir, 'det','det_yolo.txt')
        # force video_path to none
        video_path = None
    if output_dir is not None:
        detfile_dir = output_dir



    # model path
    cfg_file = args.cfg_file
    weights_file = args.weights_file
    meta_file = args.meta_file

    # Load the network
    net = dn.load_net(cfg_file, weights_file, 0)
    meta = dn.load_meta(meta_file)
    print('Loaded network {:s}'.format(weights_file))


    timer1 = Timer()
    frameid = 0


    exten = ['mp4','h264','asf','avi']

    video_folder = args.video
    fodername = video_folder.split(os.sep)[-1]
    #print(path_list)

    video_list = [y for x in os.walk(video_folder) for ext in exten for y in glob(os.path.join(x[0], '*.%s'% ext))]
    print("num of file: ",len(video_list))    

    #CAM_NUM = "{}_{}".format(path_list[0], path_list[1])



    #video_list = [os.path.join(video_folder, f) for f in os.listdir(video_folder)]

    output_dir = "/media/graymatics/backup/lize/dataset/Graymatics/customer_videos/20180704/Keppel_Logistics/Good_videos/Kepple_VOC"

    # Process video
    for video_path in video_list:
        path_list = video_path.split(os.sep)
        video_name = path_list[-1].split('.')[0]
        #print(video_path)
        #print(path_list)
        dir_name = os.path.join(output_dir,fodername,video_name)
        print(dir_name)
        checkfilepath(dir_name)


        print(" video is provided ...")
        # import video
        cap = cv2.VideoCapture(video_path)
        fps = (cap.get(cv2.CAP_PROP_FPS))
        print ("fps:{}".format(fps)) 
        print(int(cap.get(cv2.CAP_PROP_FRAME_COUNT)))

        videoname = os.path.splitext(os.path.basename(video_path))[0]

        #crop_dir = os.path.join(dir_name, "crop")

        object_List = ["person","truck"]
        #Class_list = ["person","truck","handbag", "backpack","suitcase","cell phone", "tvmonitor", "laptop","chair"]
        #object_List = ["handbag", "backpack","suitcase","cell phone", "tvmonitor", "laptop","chair"]
        
        anno_dir = os.path.join(dir_name,'Annotations')
        main_dir = os.path.join(dir_name,'ImageSets','Main')
        jpeg_dir = os.path.join(dir_name,'JPEGImages')
        bg_dir = os.path.join(dir_name,'Background') # image with no objects
        crop_dir = os.path.join(dir_name,'Crops')
        #shutil.os.mkdir(anno_dir)
        checkfilepath(anno_dir)
        checkfilepath(main_dir)
        checkfilepath(jpeg_dir)
        checkfilepath(bg_dir)
        checkfilepath(crop_dir)

        #f = open(all_path('ImageSets/Main/' + datatype + '.txt'), 'a')
        name_list = []
        bg_list = []

        timer1.tic()
        frameid = 0
        while True:
            frameid += 1
            #print('frame: {}'.format(frameid))
            grabbed, frame = cap.read()
            if not grabbed:
                print('display end')
                break
            
            image_name = '{}_{:06d}.jpg'.format(video_name, frameid)
            img = frame.copy()
            if frameid % 100 == 0:
                print('frame: ',frameid)
                h, w, c = img.shape
                assert c == 3
                # write xml head
                head = headstr % (image_name, w, h, c)

                r = dn.detect3_np(net, meta, img, class_=object_List, thresh=0.30)

                object_list = []
                obj_box_list = []

                for s in r:
                    if s['object'] in object_List:
                        object_list.append(s)
                        obj_box_list.append(s['box'])
              
                num_objs = len(object_list)
                obj_box = np.asarray(obj_box_list, dtype=np.float32)

                if (num_objs == 0):
                    if (frameid % 600 == 0):
                        head = headstr % ('bg_'+image_name, w, h, c)
                        objs = []
                        bg_path = os.path.join(bg_dir,'bg_'+image_name)
                        checkfilepath(bg_path)
                        cv2.imwrite(bg_path, frame, [int(cv2.IMWRITE_JPEG_QUALITY), 100])
                        xml_path = os.path.join(bg_dir, 'bg_'+image_name.split('.')[0]+'.xml')
                        writexml(xml_path, head, objs, tailstr)
                        bg_list.append('bg_'+image_name.split('.')[0])
                    continue

                if len(obj_box) == 0:
                    raise ValueError("no objects")
                    obj_box = np.zeros((0,4),dtype=np.float32)

                obj_centre = obj_box[:,:2].copy()
                
                # convert to x1 y1 x2 y2
                obj_box[:,:2] -= obj_box[:,2:4]/2.
                obj_box[:,2:4] += obj_box[:,:2]

                #objects
                objs = []
                for idx, row in enumerate(obj_box):
                    d = row.astype(np.int32)
                    #conf = s['confidence_index']
                    object_name = object_list[idx]['object']
                    label = "{}: {:.3f}%".format(object_list[idx]['object'],object_list[idx]['confidence_index'] * 100)
                    #label = "{}: {:.3f}%".format(s['object'],conf * 100)

                    bb = np.zeros(4, dtype=np.int32)
                    bb[0] = np.maximum(d[0], 0)
                    bb[1] = np.maximum(d[1], 0)
                    bb[2] = np.minimum(d[2], img.shape[1])
                    bb[3] = np.minimum(d[3], img.shape[0])
                    object_crop = frame[bb[1]:bb[3], bb[0]:bb[2]]
                    objs.append({'name':object_name,'bndbox':bb})
                    #print(person_crop.shape)
                    if(display):
                        cv2.rectangle(img,(d[0],d[1]), (d[2], d[3]), (0,0,255), 2)
                        cv2.putText(img, label,(d[0],d[1]-15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, 255, 1, cv2.LINE_AA)
                    # save object crops
                    crop_path = os.path.join(crop_dir,object_name, image_name+'_%d.jpg' % idx)
                    checkfilepath(crop_path)
                    cv2.imwrite(crop_path, object_crop, [int(cv2.IMWRITE_JPEG_QUALITY), 100])


                
                #write xml
                xml_path = os.path.join(dir_name,'Annotations', image_name.split('.')[0]+'.xml')
                checkfilepath(xml_path)
                writexml(xml_path, head, objs, tailstr)

                # save the whole frame
                save_path = os.path.join(dir_name,'JPEGImages', image_name)
                checkfilepath(save_path)
                cv2.imwrite(save_path, frame, [int(cv2.IMWRITE_JPEG_QUALITY), 100])
                #cv2.imwrite(save_path, person_crop, [int(cv2.IMWRITE_PNG_COMPRESSION), 9])
                
                name_list.append(image_name.split('.')[0])

        
            # dispaly image
            if(display):
                cv2.imshow('Display',img)
                cv2.waitKey(1)
        
        # write txt file
        from random import shuffle
        shuffle(name_list)
        shuffle(bg_list)
        total_num = len(name_list)
        train_idx = int(total_num*0.8)
        train_list = name_list[:train_idx]
        rest_ = name_list[train_idx:]
        half_idx = int(len(rest_)*0.5)
        val_list = rest_[:half_idx]
        test_list = rest_[half_idx:]
        trainval_list = train_list+val_list
        shuffle(trainval_list)


        checkfilepath(os.path.join(main_dir, 'trainval.txt'))
        writetxt(trainval_list, os.path.join(main_dir, 'trainval.txt'))
        writetxt(train_list, os.path.join(main_dir, 'train.txt'))
        writetxt(val_list, os.path.join(main_dir, 'val.txt'))
        writetxt(test_list, os.path.join(main_dir, 'test.txt'))
        writetxt(bg_list, os.path.join(main_dir, 'bg.txt'))



        timer1.toc()
        print('done!, total time: {:.3f} sec. average {:.3f} sec per frame, FPS: {:.3f}'.format(timer1.total_time, float(timer1.total_time/frameid), float(frameid/timer1.total_time)))

