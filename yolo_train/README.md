# Lize CAI 

This is the repo for training your own yolov3 detector using own dataset.


Please use AlexeyAB version of darknet.

git clone https://github.com/AlexeyAB/darknet.git

Build the darknet then modify the path in train.sh

CUDA_VISIBLE_DEVICES=1 /path to /darknet_alex/darknet detector train data/obj.data cfg/yolov3-obj.cfg darknet53.conv.74 -dont_show| tee yolo_obj_832.log
