
darknet=darknet_alexey/darknet

CUDA_VISIBLE_DEVICES=0 darknet_alexey/darknet detector test data/yolo.data cfg/yolov3_deploy.cfg backup/yolov3_train_.weights test/test.jpg -thresh 0.25

# tiny
CUDA_VISIBLE_DEVICES=0 darknet_alexey/darknet detector test data/yolo.data cfg/yolov3-tiny_deploy.cfg backup/yolov2-tiny_train_.weights test/test.jpg -thresh 0.25



