#/media/graymatics/data/lize/code/darknet_alex/darknet detector calc_anchors disk.data -num_of_clusters 9 -width 832 -height 832

darknet=darknet_alexey/darknet
cfgFile="cfg/yolov2-tiny_train.cfg"
weightsFile="weights/yolov2-tiny.conv.13"
dataFile="data/yolo.data"
DATE=`date '+%Y%m%d_%H%M%S'`
logFile="logs/yolov2_tiny_416_$DATE.log"

CUDA_VISIBLE_DEVICES=0 $darknet detector train $dataFile $cfgFile $weightsFile -dont_show| tee $logFile

python scripts/log_parser.py --log-file=$logFile --save-dir=./logs --show=True


