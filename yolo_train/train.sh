#/media/graymatics/data/lize/code/darknet_alex/darknet detector calc_anchors disk.data -num_of_clusters 9 -width 832 -height 832

darknet=darknet_alexey/darknet
cfgFile="cfg/yolov3_train.cfg"
weightsFile="weights/darknet53.conv.74"
dataFile="data/yolo.data"
DATE=`date '+%Y%m%d_%H%M%S'`
logFile="logs/yolov3_608_$DATE.log"

CUDA_VISIBLE_DEVICES=0 $darknet detector train $dataFile $cfgFile $weightsFile -dont_show| tee $logFile

#CUDA_VISIBLE_DEVICES=0,1 $darknet detector train $dataFile $cfgFile $weightsFile  -gpus 0,1 -dont_show| tee $logFile


python scripts/log_parser.py --log-file=$logFile --save-dir=./logs --show=True
